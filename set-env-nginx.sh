#!/bin/sh
if [ "x"$1 = "x" ]
then
	echo "usage: $0 <domain name> [nohttps]"
	exit 1
fi

nginx_site_example=$nginx_conf_example
if [ "$2" = "nohttps" ]; then
  nginx_site_example=$nginx_conf_nohttps_example
fi

# Read configuration values
. /etc/diaspora/diaspora-common.conf
nginx_site_private=/var/lib/diaspora-common/nginx-site-diaspora

if test -f ${nginx_site_example}; then
  sed -e "s/SERVERNAME_FIXME/$1/g" -e "s/DIASPORA_SSL_PATH_FIXME/\\/etc\\/diaspora/"\
  -e "s/DIASPORA_ROOT_FIXME/\\/usr\\/share\\/diaspora/"\
  ${nginx_site_example} > ${nginx_site_private}
  ucf --debconf-ok --three-way ${nginx_site_private} /etc/nginx/sites-available/diaspora
  ln -fs /etc/nginx/sites-available/diaspora /etc/nginx/sites-enabled/
  ucfr diaspora-common /etc/nginx/sites-available/diaspora
else
  echo "nginx.conf.example not found"
  exit 1
fi
