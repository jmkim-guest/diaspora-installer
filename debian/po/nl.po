# # Dutch translation of diaspora-installer debconf templates.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the diaspora-installer package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2015, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: diaspora-installer\n"
"Report-Msgid-Bugs-To: diaspora-installer@packages.debian.org\n"
"POT-Creation-Date: 2017-04-27 12:48+0530\n"
"PO-Revision-Date: 2017-01-24 11:35+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.6\n"

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid "Host name for this instance of Diaspora:"
msgstr "Computernaam voor dit exemplaar van Diaspora:"

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"Please choose the host name which should be used to access this instance of "
"Diaspora."
msgstr ""
"Kies de computernaam die gebruikt moet worden om toegang te krijgen tot dit "
"exemplaar van Diaspora."

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"This should be the fully qualified name as seen from the Internet, with the "
"domain name that will be used to access the pod."
msgstr ""
"Dit moet de volledige officiële naam (fully qualified name) zijn, zoals die "
"vanaf het internet waargenomen wordt, met de domeinnaam die gebruikt zal "
"worden om toegang te krijgen tot het knooppunt (pod)."

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"If a reverse proxy is used, give the hostname that the proxy server responds "
"to."
msgstr ""
"Indien een achterwaartse proxy gebruikt wordt, voer dan de computernaam in "
"waarop die proxyserver reageert."

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"This host name should not be modified after the initial setup because it is "
"hard-coded in the database."
msgstr ""
"Verander deze computernaam niet meer na de initiële configuratie, omdat hij "
"onveranderlijk in de databank vastgelegd wordt."

#. Type: note
#. Description
#: ../diaspora-common.templates:2001
msgid "PostgreSQL application password"
msgstr "Het wachtwoord voor PostgreSQL van de toepassing"

#. Type: note
#. Description
#: ../diaspora-common.templates:2001
msgid ""
"You can leave the PostgreSQL application password blank, as the \"ident\" "
"authentication method is used, allowing the diaspora user on the system to "
"connect to the Diaspora database without a password."
msgstr ""
"U kunt het door de toepassing gebruikt wachtwoord in PostgreSQL leeg laten, "
"aangezien de authenticatiemethode \"ident\" gebruikt wordt, waardoor de "
"gebruiker diaspora de toelating heeft om op het systeem zonder wachtwoord "
"verbinding te maken met de databank van Diaspora."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid "Enable https?"
msgstr "Https aanzetten?"

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid ""
"Enabling https means that an SSL/TLS certificate is required to access this "
"Diaspora instance (as Nginx will be configured to respond only to https "
"requests). A self-signed certificate is enough for local testing (and can be "
"generated using, for instance, the package easy-rsa), but will not be "
"accepted for federation with other Diaspora pods."
msgstr ""
"Https aanzetten betekent dat een SSL/TLS-certificaat vereist is om toegang "
"te krijgen tot dit exemplaar van Diaspora (aangezien Nginx zal ingesteld "
"worden om enkel https-aanvragen te beantwoorden). Een door uzelf ondertekend "
"certificaat volstaat voor lokale testdoeleinden (en kan bijvoorbeeld "
"aangemaakt worden met behulp van het pakket easy-rsa), maar het zal niet "
"aanvaard worden voor verbindingen met andere Diaspora knooppunten (pods)."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid ""
"Some certificate authorities like Let's Encrypt (letsencrypt.org), StartSSL "
"(startssl.com) offer free SSL/TLS certificates that work with Diaspora; "
"however, certificates provided by CAcert will not work with Diaspora."
msgstr ""
"Sommige certificaatautoriteiten zoals Let's Encrypt (letsencrypt.org) of "
"StartSSL (startssl.com) bieden gratis SSL/TLS-certificaten aan die werken "
"met Diaspora. Daarentegen werken certificaten die door CAcert verstrekt "
"worden niet met Diaspora."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid ""
"Nginx must be reloaded after the certificate and key files are made "
"available at /etc/diaspora/ssl. letsencrypt package may be used to automate "
"interaction with Let's Encrypt to obtain a certificate."
msgstr ""
"Nadat de bestanden met het certificaat en de sleutel beschikbaar geworden "
"zijn in /etc/diaspora/ssl, moet Nginx opnieuw geladen worden. Het pakket "
"letsencrypt kan gebruikt worden om de interactie met Let's Encrypt met het "
"oog op het verkrijgen van een certificaat te automatiseren."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
#, fuzzy
#| msgid ""
#| "You can disable https if you want to access Diaspora only locally, via "
#| "Unicorn on port 3000. If you disable https, Nginx configuration will be "
#| "skipped."
msgid ""
"You can disable https if you want to access Diaspora only locally or you "
"don't want to federate with other diaspora pods."
msgstr ""
"U kunt https uitzetten indien u enkel lokaal toegang tot Diaspora wilt via "
"Unicorn op poort 3000. Indien u https uitzet, zal de configuratie van Nginx "
"overgeslagen worden."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid "Use Let's Encrypt?"
msgstr "Let's Encrypt gebruiken?"

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Symbolic links to certificate and key created using letsencrypt package (/"
"etc/letencrypt/live) will be added to /etc/diaspora/ssl if this option is "
"selected."
msgstr ""
"Indien u deze optie selecteert, zullen in /etc/diaspora/ssl symbolische "
"koppelingen gemaakt worden naar het certificaat en de sleutel (/etc/"
"letencrypt/live) die met behulp van het pakket letsencrypt gemaakt werden."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Otherwise, certificate and key files have to be placed manually to /etc/"
"diaspora/ssl directory as '<host name>-bundle.crt' and '<host name>.key'."
msgstr ""
"Anders moeten het certificaatbestand en het sleutelbestand manueel in de "
"map /etc/diaspora/ssl geplaatst worden als '<computernaam>-bundle.crt' en "
"'<computernaam>.key'."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Nginx will be stopped, if this option is selected, to allow letsencrypt to "
"use ports 80 and 443 during domain ownership validation and certificate "
"retrieval step."
msgstr ""
"Indien deze optie geselecteerd wordt, zal Nginx gestopt worden om "
"letsencrypt toe te laten de poorten 80 en 443 te gebruiken bij het valideren "
"van het eigendomsrecht op het domein en bij het ophalen van het certificaat."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
#, fuzzy
#| msgid ""
#| "Note: letsencrypt does not have a usable nginx plugin currently, so "
#| "certificates must be renewed manually after 3 months, when current "
#| "letsencrypt certificate expire."
msgid ""
"Note: letsencrypt does not have a usable nginx plugin currently, so "
"certificates must be renewed manually after 3 months, when current "
"letsencrypt certificate expire. If you choose this option, you will also be "
"agreeing to letsencrypt terms of service."
msgstr ""
"Opmerking: momenteel heeft letsencrypt geen bruikbare plug-in voor nginx, "
"waardoor het certificaat na 3 maanden handmatig vernieuwd moet worden op het "
"ogenblik dat het huidige letsencrypt-certificaat vervalt."

#. Type: string
#. Description
#: ../diaspora-common.templates:5001
msgid "Email address for letsencrypt updates:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:5001
msgid "Please provide a valid email address for letsencrypt updates."
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid "Backup your database"
msgstr "Maak een kopie van uw databank"

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"This upgrade includes long running migrations that can take hours to "
"complete on large pods. It is adviced to take a backup of your database."
msgstr ""
"Met deze opwaardering gaan langdurige omzettingen gepaard die op belangrijke "
"knooppunten (pods) uren kunnen duren vooraleer ze voltooid zijn. Het wordt "
"aanbevolen om een reservekopie van uw databank te maken."

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"Commands to backup and restore database is given below (run as root user):"
msgstr ""
"De opdrachten om een reservekopie van de databank te maken en die terug te "
"zetten zijn de volgende (voer ze uit als systeembeheerder):"

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"# su postgres -c 'pg_dump diaspora_production -f /var/lib/postgresql/"
"diaspora_production.sql'"
msgstr ""
"# su postgres -c 'pg_dump diaspora_production -f /var/lib/postgresql/"
"diaspora_production.sql'"

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"# su postgres -c 'psql -d diaspora_production -f /var/lib/postgresql/"
"diaspora_production.sql'"
msgstr ""
"# su postgres -c 'psql -d diaspora_production -f /var/lib/postgresql/"
"diaspora_production.sql'"

#. Type: multiselect
#. Description
#: ../diaspora-common.templates:7001
msgid "Third party services to be enabled: "
msgstr "Diensten van derden die geactiveerd moeten worden:"

#. Type: multiselect
#. Description
#: ../diaspora-common.templates:7001
msgid "Diaspora can connect with different services."
msgstr "Diaspora kan gekoppeld worden aan verschillende diensten."

#. Type: multiselect
#. Description
#: ../diaspora-common.templates:7001
msgid ""
"When a diaspora instance is connected to a third party service,  it allows "
"any user of this diaspora instance to link their account on that service to "
"their diaspora account and send their updates to that service if they choose "
"the service when publishing a post."
msgstr ""
"Als een exemplaar van diaspora gekoppeld wordt aan een dienst van derden, "
"dan laat dit elke gebruiker van dat exemplaar van diaspora toe om zijn "
"account bij die dienst te koppelen aan zijn account bij diaspora en zijn "
"updates naar die dienst te sturen als bij het posten van een bericht die "
"dienst gekozen wordt."

#. Type: string
#. Description
#: ../diaspora-common.templates:8001
msgid "Facebook App ID:"
msgstr "Facebook App ID:"

#. Type: string
#. Description
#: ../diaspora-common.templates:8001
msgid "Give your Facebook App ID. This can not be blank."
msgstr "Geef uw Facebook App ID. Dit mag niet leeg zijn."

#. Type: password
#. Description
#: ../diaspora-common.templates:9001
msgid "Facebook Secret:"
msgstr "Facebook-wachtwoord:"

#. Type: password
#. Description
#: ../diaspora-common.templates:9001
msgid "Give your Facebook Secret. This can not be blank."
msgstr "Geef uw wachtwoord bij Facebook. Dit mag niet leeg zijn."

#. Type: string
#. Description
#: ../diaspora-common.templates:10001
msgid "Twitter Key:"
msgstr "Twitter-sleutel:"

#. Type: string
#. Description
#: ../diaspora-common.templates:10001
msgid "Give your Twitter Key. This can not be blank."
msgstr "Geef uw Twitter-sleutel. Dit mag niet leeg zijn."

#. Type: password
#. Description
#: ../diaspora-common.templates:11001
msgid "Twitter Secret:"
msgstr "Twitter-wachtwoord:"

#. Type: password
#. Description
#: ../diaspora-common.templates:11001
msgid "Give your Twitter Secret. This can not be blank."
msgstr "Geef uw Twitter-wachtwoord. Dit mag niet leeg zijn."

#. Type: string
#. Description
#: ../diaspora-common.templates:12001
msgid "Tumblr Key:"
msgstr "Tumblr-sleutel:"

#. Type: string
#. Description
#: ../diaspora-common.templates:12001
msgid "Give your Tumblr Key. This can not be blank."
msgstr "Geef uw Tumblr-sleutel. Dit mag niet leeg zijn."

#. Type: password
#. Description
#: ../diaspora-common.templates:13001
msgid "Tumblr Secret:"
msgstr "Tumblr-wachtwoord:"

#. Type: password
#. Description
#: ../diaspora-common.templates:13001
msgid "Give your Tumblr Secret. This can not be blank."
msgstr "Geef uw Tumblr-wachtwoord. Dit mag niet leeg zijn."

#. Type: string
#. Description
#: ../diaspora-common.templates:14001
msgid "Wordpress Client ID:"
msgstr "Wordpress Client ID:"

#. Type: string
#. Description
#: ../diaspora-common.templates:14001
msgid "Give your Wordpress Client ID. This can not be blank."
msgstr "Geef uw Wordpress Client ID. Dit mag niet leeg zijn."

#. Type: password
#. Description
#: ../diaspora-common.templates:15001
msgid "Wordpress Secret:"
msgstr "Wordpress-wachtwoord:"

#. Type: password
#. Description
#: ../diaspora-common.templates:15001
msgid "Give your Wordpress Secret. This can not be blank."
msgstr "Geef uw Wordpress-wachtwoord. Dit mag niet leeg zijn."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:16001
msgid "Remove all data?"
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:16001
msgid ""
"This will permanently remove all data of this Diaspora instance such as "
"uploaded files and any customizations in homepage."
msgstr ""

#~ msgid ""
#~ "Set the hostname of the machine you're running Diaspora on, as seen from "
#~ "the internet. This should be the domain name you want to use to access "
#~ "the pod. So if you plan to use a reverse proxy, it should be the hostname "
#~ "the proxy listens on. DO NOT CHANGE THIS AFTER INITIAL SETUP! If you do "
#~ "change the hostname, you will have to start again as the hostname will be "
#~ "hardcoded into the database."
#~ msgstr ""
#~ "Stel de naam in van de computer waarop u Diaspora uitvoert, zoals die "
#~ "vanaf het internet waargenomen wordt. Dit moet de domeinnaam zijn die u "
#~ "wenst te gebruiken om verbinding te maken met het knooppunt (pod). Dus "
#~ "indien u gebruik wilt maken van een achterwaartse proxy, moet het de "
#~ "computernaam zijn waarop de proxyserver luistert. Waarschuwing: verander "
#~ "dit niet meer na de initiële configuratie! Indien u de computernaam "
#~ "wijzigt, zult u opnieuw moeten beginnen omdat de computernaam "
#~ "onveranderlijk in de database vastgelegd wordt."

#~ msgid "Examples: example.com, pod.example.com etc"
#~ msgstr "Voorbeelden: example.com, pod.example.com enzovoort"
