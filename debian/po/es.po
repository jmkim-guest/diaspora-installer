# diaspora-installer po-debconf translation to Spanish.
# Copyright (C) 2015 Software in the Public Interest
# This file is distributed under the same license as the diaspora-installer package.
#
# Changes:
# - Initial translation
# Jonathan Bustillos <jathanblackred@openmailbox.com>, 2015.
#
# Traductores, si no conocen el formato PO, merece la pena leer la
# documentación de gettext, especialmente las secciones dedicadas a este
# formato, por ejemplo ejecutando:
# info -n '(gettext)PO Files'
# info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
# - El proyecto de traducción de Debian al español
# http://www.debian.org/intl/spanish/
# especialmente las notas y normas de traducción en
# http://www.debian.org/intl/spanish/notas
#
# - La guía de traducción de po's de debconf:
# /usr/share/doc/po-debconf/README-trans
# o http://www.debian.org/intl/l10n/po-debconf/README-trans
msgid ""
msgstr ""
"Project-Id-Version: diaspora-installer\n"
"Report-Msgid-Bugs-To: diaspora-installer@packages.debian.org\n"
"POT-Creation-Date: 2017-04-27 12:48+0530\n"
"PO-Revision-Date: 2015-05-12 11:06-0500\n"
"Last-Translator: Jonathan Bustillos <jathanblackred@openmailbox.com>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.6\n"

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid "Host name for this instance of Diaspora:"
msgstr "Nombre de equipo para esta instancia de Diaspora:"

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"Please choose the host name which should be used to access this instance of "
"Diaspora."
msgstr ""
"Elija el nombre de equipo que se debe utilizar para acceder a esta instancia "
"de Diaspora."

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"This should be the fully qualified name as seen from the Internet, with the "
"domain name that will be used to access the pod."
msgstr ""
"Debe ser el nombre completamente cualificado como se ve a través de "
"Internet, con el nombre de dominio que se utilizará para acceder al «pod»."

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"If a reverse proxy is used, give the hostname that the proxy server responds "
"to."
msgstr ""
"Si se utiliza un proxy inverso, introduzca el nombre de equipo del servidor "
"proxy."

#. Type: string
#. Description
#: ../diaspora-common.templates:1001
msgid ""
"This host name should not be modified after the initial setup because it is "
"hard-coded in the database."
msgstr ""
"El nombre de equipo no debe ser modificado después de la configuración "
"inicial ya que está codificado en la base de datos."

#. Type: note
#. Description
#: ../diaspora-common.templates:2001
msgid "PostgreSQL application password"
msgstr "Introduzca la contraseña de PostgreSQL."

#. Type: note
#. Description
#: ../diaspora-common.templates:2001
msgid ""
"You can leave the PostgreSQL application password blank, as the \"ident\" "
"authentication method is used, allowing the diaspora user on the system to "
"connect to the Diaspora database without a password."
msgstr ""
"Puede dejar la contraseña de PostgreSQL en blanco, ya que se utiliza el "
"método de autenticación \"ident\", que permite al usuario de diaspora "
"conectarse a la base de datos de Diaspora sin contraseña."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid "Enable https?"
msgstr "¿Activar https?"

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
#, fuzzy
#| msgid ""
#| "Enabling https means that an SSL certificate is required to access this "
#| "Diaspora instance (as Nginx will be configured to respond only to https "
#| "requests). A self-signed certificate is enough for local testing (and can "
#| "be generated using, for instance, the package easy-rsa), but will not be "
#| "accepted for federation with other Diaspora pods."
msgid ""
"Enabling https means that an SSL/TLS certificate is required to access this "
"Diaspora instance (as Nginx will be configured to respond only to https "
"requests). A self-signed certificate is enough for local testing (and can be "
"generated using, for instance, the package easy-rsa), but will not be "
"accepted for federation with other Diaspora pods."
msgstr ""
"Activar https significa que se requiere un certificado SSL para acceder a "
"esta instancia de Diaspora (como Nginx se configurará sólo para responder a "
"las solicitudes https). Un certificado autofirmado es suficiente para una "
"prueba local (y se puede generar utilizando, por ejemplo, el paquete easy-"
"rsa), pero no será aceptado para la federación con otros «pods» de Diaspora."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
#, fuzzy
#| msgid ""
#| "Some certificate authorities like StartSSL (startssl.com) or WoSign (buy."
#| "wosign.com/free) offer free SSL certificates that work with Diaspora; "
#| "however, certificates provided by CAcert will not work with Diaspora."
msgid ""
"Some certificate authorities like Let's Encrypt (letsencrypt.org), StartSSL "
"(startssl.com) offer free SSL/TLS certificates that work with Diaspora; "
"however, certificates provided by CAcert will not work with Diaspora."
msgstr ""
"Algunas autoridades de certificación como StartSSL (startssl.com) o WoSign "
"(buy.wosign.com/free) ofrecen certificados SSL gratis que trabajan con "
"Diáspora; Sin embargo, los certificados proporcionados por CAcert no "
"funcionarán con Diaspora."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
msgid ""
"Nginx must be reloaded after the certificate and key files are made "
"available at /etc/diaspora/ssl. letsencrypt package may be used to automate "
"interaction with Let's Encrypt to obtain a certificate."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:3001
#, fuzzy
#| msgid ""
#| "You can disable https if you want to access Diaspora only locally, via "
#| "Unicorn on port 3000. If you disable https, Nginx configuration will be "
#| "skipped."
msgid ""
"You can disable https if you want to access Diaspora only locally or you "
"don't want to federate with other diaspora pods."
msgstr ""
"Puede desactivar https si desea acceder a Diaspora sólo a nivel local, a "
"través de «Unicorn» en el puerto 3000. Si desactiva https, se omitirá la "
"configuración de Nginx."

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid "Use Let's Encrypt?"
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Symbolic links to certificate and key created using letsencrypt package (/"
"etc/letencrypt/live) will be added to /etc/diaspora/ssl if this option is "
"selected."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Otherwise, certificate and key files have to be placed manually to /etc/"
"diaspora/ssl directory as '<host name>-bundle.crt' and '<host name>.key'."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Nginx will be stopped, if this option is selected, to allow letsencrypt to "
"use ports 80 and 443 during domain ownership validation and certificate "
"retrieval step."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:4001
msgid ""
"Note: letsencrypt does not have a usable nginx plugin currently, so "
"certificates must be renewed manually after 3 months, when current "
"letsencrypt certificate expire. If you choose this option, you will also be "
"agreeing to letsencrypt terms of service."
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:5001
msgid "Email address for letsencrypt updates:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:5001
msgid "Please provide a valid email address for letsencrypt updates."
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid "Backup your database"
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"This upgrade includes long running migrations that can take hours to "
"complete on large pods. It is adviced to take a backup of your database."
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"Commands to backup and restore database is given below (run as root user):"
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"# su postgres -c 'pg_dump diaspora_production -f /var/lib/postgresql/"
"diaspora_production.sql'"
msgstr ""

#. Type: note
#. Description
#: ../diaspora-common.templates:6001
msgid ""
"# su postgres -c 'psql -d diaspora_production -f /var/lib/postgresql/"
"diaspora_production.sql'"
msgstr ""

#. Type: multiselect
#. Description
#: ../diaspora-common.templates:7001
msgid "Third party services to be enabled: "
msgstr ""

#. Type: multiselect
#. Description
#: ../diaspora-common.templates:7001
msgid "Diaspora can connect with different services."
msgstr ""

#. Type: multiselect
#. Description
#: ../diaspora-common.templates:7001
msgid ""
"When a diaspora instance is connected to a third party service,  it allows "
"any user of this diaspora instance to link their account on that service to "
"their diaspora account and send their updates to that service if they choose "
"the service when publishing a post."
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:8001
msgid "Facebook App ID:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:8001
msgid "Give your Facebook App ID. This can not be blank."
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:9001
msgid "Facebook Secret:"
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:9001
msgid "Give your Facebook Secret. This can not be blank."
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:10001
msgid "Twitter Key:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:10001
msgid "Give your Twitter Key. This can not be blank."
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:11001
msgid "Twitter Secret:"
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:11001
msgid "Give your Twitter Secret. This can not be blank."
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:12001
msgid "Tumblr Key:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:12001
msgid "Give your Tumblr Key. This can not be blank."
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:13001
msgid "Tumblr Secret:"
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:13001
msgid "Give your Tumblr Secret. This can not be blank."
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:14001
msgid "Wordpress Client ID:"
msgstr ""

#. Type: string
#. Description
#: ../diaspora-common.templates:14001
msgid "Give your Wordpress Client ID. This can not be blank."
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:15001
msgid "Wordpress Secret:"
msgstr ""

#. Type: password
#. Description
#: ../diaspora-common.templates:15001
msgid "Give your Wordpress Secret. This can not be blank."
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:16001
msgid "Remove all data?"
msgstr ""

#. Type: boolean
#. Description
#: ../diaspora-common.templates:16001
msgid ""
"This will permanently remove all data of this Diaspora instance such as "
"uploaded files and any customizations in homepage."
msgstr ""
